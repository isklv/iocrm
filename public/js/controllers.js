angular.module('app.controller', ['linkify'])
    .controller('InitCtrl', function($scope, $cookies, $location, $routeParams, Utf8, socket)                                                                                                                                                                                           {
        
        $scope.onlineUsers = [];
        $scope.newMessages = [];
        
        $scope.init = function(){
            if($cookies.login){
                socket.emit('online', $cookies.login);
            }else if($location.url() !== '/login'){
                $location.path("/login");
            }

        }
        
        $scope.logout = function(){
            
            socket.emit('logout', $cookies.login);
            
            delete $cookies.login;
            delete $cookies.firstname;
            delete $cookies.lastname;
            delete $cookies.avatar;

            $location.path("/login");
        }
        
         socket.on('msg:private', function(data){

            if(data.from._id != $routeParams.userId || !$scope.userActive){   
                $scope.newMessages.push(JSON.parse(Utf8.decode(JSON.stringify(data))));
                var msg = new Audio;
                msg.src = '/sounds/msg.mp3';
                msg.play();
            }

        });
    }) 
    .controller('LoginCtrl', function($scope, AuthService, $location, $cookies){
        if($cookies.login)
            $location.path('/user/'+$cookies.login);
                
        $scope.user = {
            email: '',
            password: '',
            remember: false
        };

        $scope.login = function(){
            AuthService.login($scope.user);
        }
    })
    .controller('UserCtrl', function($scope, $http, $location, $routeParams, $cookies, Gravatar){
        
        $scope.getUser = function(){
            $http({
                method: 'GET',
                url: '/api/users/'+$routeParams.userId,
                params: {access_token: $cookies.token},
                headers: {'Content-Type': 'application/json'}
            }).
            success(function(data, status, headers, config) {
                $scope.lastname = data.lastname;
                $scope.firstname = data.firstname;
                $scope.group = data.group;
                $scope.email = data.email;
                $scope.avatar = Gravatar(data.email);
            })
        }
        
        $scope.getUsers = function(){
            $http({
                method: 'GET',
                url: '/api/users/',
                params: {access_token: $cookies.token},
                headers: {'Content-Type': 'application/json'}
            }).
            success(function(data, status, headers, config) {
                $scope.users = data;
            })
        }
        
        $scope.getAvatar = function(email){
            return Gravatar(email);
        }
        
        $scope.redirect = function(id){
            $location.path('/user/' + id);
        }
        
        $scope.getGroups = function(){
            $http({
                method: 'GET',
                url: '/api/groups/',
                params: {access_token: $cookies.token},
                headers: {'Content-Type': 'application/json'}
            }).
            success(function(data, status, headers, config) {
                $scope.groups = data;
            })
        }
        
        $scope.addUser = function(){
            var data = {
                lastname: $scope.lastname,
                firstname: $scope.firstname,
                email: $scope.email,
                password: $scope.password,
                group: $scope.usergroup
            }
            
            $http({
                method: 'POST',
                url: '/api/users/',
                params: {access_token: $cookies.token},
                headers: {'Content-Type': 'application/json'},
                data: data
            }).
            success(function(data, status, headers, config) {
                $scope.getUsers();
            })
        }
         
        $scope.newMessage = function(event, id){
            event.preventDefault();
            event.stopPropagation();
            
            $location.path('/msgto/' + id);
        }
    })
    .controller('RegCtrl', function($scope, $http, $location){
        $scope.user = {
            email: '',
            password: '',
            lastname: '',
            firstname: ''
        };

        $scope.registration = function(user){

            $http({
                method: 'POST',
                url: '/api/users',
                data: user,
                headers: {'Content-Type': 'application/json'}
            }).success(function(data, status, headers, config) {
                $location.path('login');
            });
        }
    })
    .controller('MessagesCtrl', function($scope, $http, $cookies, $routeParams, Gravatar, Utf8, socket){
        
        $scope.messages = [];
        
        $scope.newMessageInit = function(){
            
            //delete readed message
            for(var i = 0; i < $scope.newMessages.length; i++){
                if($scope.newMessages[i].from._id == $routeParams.userId){
                    $scope.newMessages.splice(i, 1);  
                    i--;
                }
            }
            
            //get user dialog
            $http({
                method: 'GET',
                url: '/api/users/'+$routeParams.userId,
                headers: {'Content-Type': 'application/json'},
                params: {access_token: $cookies.token},
            }).
            success(function(data, status, headers, config) {
                $scope.user = {}
                
                $scope.user._id = data._id;
                $scope.user.lastname = data.lastname;
                $scope.user.firstname = data.firstname;
                $scope.user.group = data.group;
                $scope.user.email = data.email;
                $scope.user.avatar = Gravatar(data.email); 
            })
            
            //get messages
            $http({
                method: 'GET',
                url: '/api/msg/' + $cookies.login + '/' + $routeParams.userId,
                headers: {'Content-Type': 'application/json'},
                params: {access_token: $cookies.token},
            }).
            success(function(data, status, headers, config) {
                $scope.messages = data;
            })

        }
        
        $scope.addMessage = function(){
            if($scope.msg)
                socket.emit('msg:send', {text: Utf8.encode($scope.msg), to: $routeParams.userId, from: $cookies.login});
            
            $scope.msg = '';
        }
        
        socket.on('msg:get', function(data){          
            $scope.messages.push(JSON.parse(Utf8.decode(JSON.stringify(data))));  
        });

        socket.on('msg:private', function(data){
            
            if(data.from._id == $routeParams.userId){
                
                if(!$scope.userActive){
                    data.nread = true;
                } else{
                    var msg = new Audio;
                    msg.src = '/sounds/msg.mp3';
                    msg.play();
                }              
                
                $scope.messages.push(JSON.parse(Utf8.decode(JSON.stringify(data))));
                
            }
        });

    })
    .controller('GroupsCtrl', function($scope, $http, $cookies){
        $scope.getGroups = function(){
            $http({
                method: 'GET',
                url: '/api/groups/',
                params: {access_token: $cookies.token},
                headers: {'Content-Type': 'application/json'}
            }).
            success(function(data, status, headers, config) {
                $scope.groups = data;
            })
        }
        //$scope.show = false;
        $scope.addGroup = function(){
            $http({
                method: 'POST',
                url: '/api/groups/',
                headers: {'Content-Type': 'application/json'},
                params: {access_token: $cookies.token},
                data: {title: $scope.title}
            }).
            success(function(data, status, headers, config) {
                $scope.getGroups();
            })
        }
        
        
    })
    .controller('ToDoCtrl', function($scope, $http, $cookies){
        
        $scope.events = [];
        
        $scope.getEvents = function(){
             $http({
                method: 'GET',
                url: '/api/todo/' + $cookies.login,
                params: {access_token: $cookies.token},
                headers: {'Content-Type': 'application/json'}
            }).
            success(function(data, status, headers, config) {
                $scope.events = data;
            })
        }
                
        $scope.addEvent = function(){
            var data = {
                text: $scope.text,
                _user: $cookies.login           
            }
            $http({
                method: 'POST',
                url: '/api/todo/',
                params: {access_token: $cookies.token},
                headers: {'Content-Type': 'application/json'},
                data: data
            }).
            success(function(data, status, headers, config) {
                $scope.events.push(data);
                $scope.text = "";
            })
        }
        
        $scope.edit = function(){
            var _id = this.event;
            var data = {
                text: _id.text,
                status: _id.status
            }
            $http({
                method: 'PUT',
                url: '/api/todo/' + _id._id,
                params: {access_token: $cookies.token},
                headers: {'Content-Type': 'application/json'},
                data: data
            }).
            success(function(data, status, headers, config) {
                //$scope.events.push(data);
                //$scope.events[$scope.events.indexOf(_id)] = _id;
            })
        }
        
        $scope.delete = function(){
            var _id = this.event;

            $http({
                method: 'DELETE',
                url: '/api/todo/' + _id._id,
                params: {access_token: $cookies.token},
                headers: {'Content-Type': 'application/json'},
            }).
            success(function(data, status, headers, config) {
                $scope.events.splice($scope.events.indexOf(_id), 1);
            })
        }
    });