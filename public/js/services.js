angular.module('app.services', ['ngCookies', 'ngMd5', 'btford.socket-io'])
    .factory('socket', function (socketFactory) {
        return socketFactory({
            ioSocket: io.connect()
        });
    })
    .factory('AuthService', function($cookies, $location, $http, md5, socket, Gravatar){
        var authService = {};
        
        authService.login = function(user){
            
            user.client_id = navigator.userAgent;
            
            $http({
                method: 'POST',
                url: '/api/login',
                data: user,
                headers: {'Content-Type': 'application/json'}
            }).
            success(function(data, status, headers, config) {
                $cookies.login = data.user._id;
                $cookies.firstname = data.user.firstname;
                $cookies.lastname = data.user.lastname;
                $cookies.token = data.user.hashedPassword;
                $cookies.avatar = Gravatar(data.user.email);
                
                socket.emit('online', $cookies.login);
                
                $location.path("/user/"+$cookies.login);
            }).
            error(function(data, status, headers, config) {
                console.log('Not validate');
                return false;
            });
            
            
            
        }
        
        return authService;
    })
    .factory('Gravatar', function(md5){
        return function(email){
            var service = email.split('@')[1];
            switch(service){
                case 'yandex.ru':
                case 'ya.ru':
                case 'mail.ru':
                case 'inbox.ru':
                case 'bk.ru':
                    return 'https://filin.mail.ru/pic?width=90&height=90&email='+escape(email);
                break;
                default:
                    return 'http://www.gravatar.com/avatar/' + md5.createHash(email.toLowerCase());
            }
            
        }
    })
    .factory('Utf8', function(){
         var Utf8 = {
            encode : function (string) {
                string = string.replace(/rn/g,"n");
                var utftext = "";
                for (var n = 0; n < string.length; n++) {
                    var c = string.charCodeAt(n);
                    if (c < 128) {
                        utftext += String.fromCharCode(c);
                    }
                    else if((c > 127) && (c < 2048)) {
                        utftext += String.fromCharCode((c >> 6) | 192);
                        utftext += String.fromCharCode((c & 63) | 128);
                    }
                    else {
                        utftext += String.fromCharCode((c >> 12) | 224);
                        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                        utftext += String.fromCharCode((c & 63) | 128);
                    }
                }
                return utftext;
            },
            decode : function (utftext) {
                var string = "";
                var i = 0;
                var c = c1 = c2 = 0;
                while ( i < utftext.length ) {
                    c = utftext.charCodeAt(i);
                    if (c < 128) {
                        string += String.fromCharCode(c);
                        i++;
                    }
                    else if((c > 191) && (c < 224)) {
                        c2 = utftext.charCodeAt(i+1);
                        string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                        i += 2;
                    }
                    else {
                        c2 = utftext.charCodeAt(i+1);
                        c3 = utftext.charCodeAt(i+2);
                        string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                        i += 3;
                    }
                }
                return string;
            }
        }
        
        return Utf8;
    })
    .directive('profile', ['$interval', '$cookies', function($interval, $cookies){
        return function(scope, element, attrs){
            
            scope.$watch(function(){
                    return $cookies;
                },
                function(newValue, oldValue){
                    if(newValue.login)
                        element.html("<img class='th' src='"+newValue.avatar+"'><h4>" + newValue.firstname + " " + newValue.lastname + "</h4>");
            }, true);
            
        }
    }])
    .directive('init', ['$window', function ($window) {
        return function (scope, element) {
                                         
            scope.$watch(function(){
                    return {
                            h: $window.innerHeight,
                            w: $window.innerWidth
                        };
                }, 
                function (newValue, oldValue) {
                    scope.windowHeight = newValue.h;
                    scope.windowWidth = newValue.w;

                    scope.style = function () {
                        return {
                            'height': (newValue.h - 300) + 'px'
                        };
                    };

            }, true);
            
            angular.element($window).bind('resize', function () {
                scope.$apply();
            });
            
            angular.element($window).bind('blur', function(){
                scope.userActive = false;
                scope.$apply();
            })
            angular.element($window).bind('focus', function(){              
                scope.userActive = true;
                scope.$apply();
            });    
        }
    }])
    .directive('onlineLbl', ['socket', function(socket){
        return function (scope, element, attrs) {
            
            scope.$watch('usersOnline.length', function (newValue, oldValue) {
                if(newValue && scope.usersOnline.indexOf(attrs.onlineLbl) !== -1)
                    element.html('<span class="success radius label right">В сети</span>');
                else
                    element.empty();
            }, true);
            
            socket.on('users:online', function(data){
                scope.usersOnline = data;
                scope.$apply();
            });
        }
    }])
    .directive('messages', ['$timeout', '$http', '$cookies', '$routeParams', '$window', function($timeout, $http, $cookies, $routeParams, $window){
        return function (scope, element, attrs) {
            scope.$watch('messages', function (newValue, oldValue) {
                $timeout(function () {
                    document.getElementById('bottom').scrollIntoView();
                });
            }, true);       
            
            scope.$watch('userActive', function(newValue){
                $timeout(function(){
                    if(newValue && scope.newMessages.length){
                        for(var i = 0; i < scope.newMessages.length; i++){
                            if(scope.newMessages[i].to == $cookies.login && scope.newMessages[i].from._id == $routeParams.userId){
                                scope.newMessages.splice(i, 1);
                                i--;
                            }                       
                        }
                        for(var i = scope.messages.length - 1; i >= 0; i--){
                            if(scope.messages[i].nread){
                                delete scope.messages[i].nread;
                            } 
                        }
                        scope.$apply();
                    }
                });
            }, true);
                
            
            element.bind('scroll', function(){
                if(this.scrollTop === 0){

                    $http({
                        method: 'GET',
                        url: '/api/msg/' + $cookies.login + '/' + $routeParams.userId + '/' + scope.messages.length,
                        headers: {'Content-Type': 'application/json'}
                    }).
                    success(function(data, status, headers, config) {
                        var old = scope.messages;
                        scope.messages = data.concat(old);
                        scope.$apply();
                    })
                }
            });
        }
    }])
    .directive('date', function(){
        return function (scope, element, attrs) {
            var d = new Date(attrs.date);
            var today = new Date();
            var months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
            
            var strday = "";
                    
            if(d.getDate() != today.getDate())
                strday += d.getDate() + ' ' + months[d.getMonth()] + ' ';
            
            if(d.getFullYear() != today.getFullYear())
                strday += d.getFullYear() + ' ';

            var minutes = (d.getMinutes().toString().length == 1)? '0'+d.getMinutes() : d.getMinutes();
            var seconds = (d.getSeconds().toString().length == 1)? '0'+d.getSeconds() : d.getSeconds();
            
            strday += d.getHours() + ':' + minutes + ':' + seconds;
            
            element.html(strday)
        }
    })
    .directive('newMessage', ['Gravatar', function(Gravatar){
        return function(scope, element){
            
            scope.$watch('newMessages', function(newValue){
                
                var html = '';
                for(var i = 0; i < newValue.length; i++){
                    if(i == 0 || newValue[i].from._id !== newValue[i-1].from._id)
                        html += '<a href="#/msgto/'+newValue[i].from._id+'"><div data-alert class="alert-box secondary radius"><img width="40" class="th" src="'+Gravatar(newValue[i].from.email)+'"><div style="float:right;padding:10px">Новое сообщение.</div></div></a>'
                }
                element.html(html);
            }, true)
        }
    }])
    .directive('loadFile', ['$cookies', '$routeParams', 'socket', 'Utf8', function($cookies, $routeParams, socket, Utf8){
        return function(scope, element){
            
            var a = {"Ё":"YO","Й":"I","Ц":"TS","У":"U","К":"K","Е":"E","Н":"N","Г":"G","Ш":"SH","Щ":"SCH","З":"Z","Х":"H","Ъ":"","ё":"yo","й":"i","ц":"ts","у":"u","к":"k","е":"e","н":"n","г":"g","ш":"sh","щ":"sch","з":"z","х":"h","ъ":"","Ф":"F","Ы":"I","В":"V","А":"a","П":"P","Р":"R","О":"O","Л":"L","Д":"D","Ж":"ZH","Э":"E","ф":"f","ы":"i","в":"v","а":"a","п":"p","р":"r","о":"o","л":"l","д":"d","ж":"zh","э":"e","Я":"Ya","Ч":"CH","С":"S","М":"M","И":"I","Т":"T","Ь":"","Б":"B","Ю":"YU","я":"ya","ч":"ch","с":"s","м":"m","и":"i","т":"t","ь":"","б":"b","ю":"yu"};

            function transliterate(word){
                return word.split('').map(function (char) { 
                    return (a[char] || a[char] === "")? a[char] : char; 
                }).join("").replace(' ', '_');
            }
            
            element.bind('click', function(){
                document.getElementById('browseFile').click();
            });
            
            angular.element(document.getElementById('browseFile')).on('change',function(e){

                var file = e.target.files[0];

                angular.element(document.getElementById('browseFile')).val('');

                var fileReader = new FileReader();

                fileReader.onload=function(event){
                    socket.emit('msg:send', {text: Utf8.encode(event.target.result), to: $routeParams.userId, from: $cookies.login, type: 'file', filename: Utf8.encode(transliterate(file.name))});
                }
                fileReader.readAsBinaryString(file);
            });
        }
    }])
    .directive('messageText', ['$timeout', function($timeout){
        return function(scope, element){
            
            $timeout(function () { 
                //var html = element.html(); 
                //element.html(html);
                
            });
            
            element.bind('mouseenter', function(){
                element.addClass('hover');
            })
            
            element.bind('mouseout', function(){
                element.removeClass('hover');
            })
        }
    }])
    .directive('title', function(){
        return {
            restrict: 'E',
            link: function(scope, element){
                scope.$watch('newMessages', function(newValue){
                    var count = (newValue.length)? '('+newValue.length+') ' : '';
                    element.html(count + 'ООО "НИИТОНХиБТ"');
                }, true);
            }
        }
    })
    .directive('footer', function(){
        return {
            restrict: 'E',
            link: function(scope, element){
                element.html('<small>&copy; 2014, ООО "НИИТОНХиБТ" v0.0.1a</small>');
            }
        }
    })