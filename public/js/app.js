var app = angular.module('app', ['ngRoute', 'app.controller', 'app.services']).
    config(['$routeProvider', 
        function($routeProvider) {
            $routeProvider.
            when('/404', {
                templateUrl: 'views/404.html',
                }).
            when('/login', {
                templateUrl: 'views/login.html',
                controller: 'LoginCtrl'}).
            when('/users', {
                templateUrl: 'views/users.html',
                controller: 'UserCtrl'}).
            when('/clients', {
                templateUrl: 'views/clients.html',
                controller: 'ClientsCtrl'}).
            when('/groups', {
                templateUrl: 'views/groups.html',
                controller: 'GroupsCtrl'}).
            when('/todo', {
                templateUrl: 'views/todo.html',
                controller: 'ToDoCtrl'}).
            when('/messages', {
                templateUrl: 'views/messages.html',
                controller: 'MessagesCtrl'}).
            when('/registration', {
                templateUrl: 'views/registration.html',
                controller: 'RegCtrl'}).
            when('/user/:userId', {
                templateUrl: 'views/user.html', 
                controller: 'UserCtrl'
            }).
            when('/msgto/:userId', {
                templateUrl: 'views/message.html', 
                controller: 'UserCtrl'
            }).
            otherwise({
                redirectTo: '/404'
            });
        }]).
    config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }]);