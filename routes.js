var db          = require('./libs/mongoose');
var passport    = require('passport');
var crypto      = require('crypto');

module.exports.setup = function (app, handlers) {
    
    app.get('/api/users', passport.authenticate('bearer', { session: false }), handlers.user.list);
    app.get('/api/users/:id', passport.authenticate('bearer', { session: false }), handlers.user.get);
    app.post('/api/users', passport.authenticate('bearer', { session: false }), handlers.user.create);
    app.put('/api/users/:id', passport.authenticate('bearer', { session: false }), handlers.user.update);
    app.delete('/api/users/:id', passport.authenticate('bearer', { session: false }), handlers.user.remove);
    app.post('/api/login', function(req, res) {
        return db.model('user').findOne({email: new RegExp(req.body.email, "i")}, function (err, user) {
            if(!user || !user.checkPassword(req.body.password)) {
                res.statusCode = 404;
                return res.send({ status: 'ERROR', error: 'Not found' });
            }
            if (!err) {     
                user.hashedPassword = '';
                user.salt = '';
                db.model('accesstoken').findOne({userId: user._id}, function(err, token){
                    if(!token){
                        db.model('accesstoken').create({clientId: req.body.client_id, userId: user._id, token: crypto.randomBytes(32).toString('base64')}, function(err, data){
                            user.hashedPassword = data.token;
                            res.send({ status: 'OK', user:user });
                        });
                    }else{
                        user.hashedPassword = token.token;
                        res.send({ status: 'OK', user:user });
                    }
                });
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s', res.statusCode, err.message);
                return res.send({ error: 'Server error' });
            }
        });
    });
    
    app.get('/api/groups', passport.authenticate('bearer', { session: false }), handlers.usergroup.list);
    app.post('/api/groups', passport.authenticate('bearer', { session: false }), handlers.usergroup.create);
    
    app.get('/api/todo/:id', passport.authenticate('bearer', { session: false }), function (req, res, next) {
        db.model('todo').find({_user: req.params.id}, function (err, data) {
          if (err) next(err);
          res.send(data);
        });
    });
    app.post('/api/todo', passport.authenticate('bearer', { session: false }), handlers.todo.create);
    app.put('/api/todo/:id', passport.authenticate('bearer', { session: false }), handlers.todo.update);
    app.delete('/api/todo/:id', passport.authenticate('bearer', { session: false }), handlers.todo.remove);
    
    var algorithm = 'aes-256-ctr';
    var password = 'jJHhj3K';
    
    app.get('/api/msg/:from/:to', passport.authenticate('bearer', { session: false }), function (req, res, next) {
        db.model('messages').count({$or: [
                {$and: [{to: req.params.from}, {from: req.params.to}]}, 
                {$and: [{from: req.params.from}, {to: req.params.to}]}
            ]}, function(err, data){
                var skip = data < 40 ? 0: data - 40;
                
                db.model('messages').find({$or: [
                    {$and: [{to: req.params.from}, {from: req.params.to}]}, 
                    {$and: [{from: req.params.from}, {to: req.params.to}]}
                ]}).populate('from').skip(skip).exec(function (err, data) {
                    if (err) next(err);
                    if(data){
                        for(var i = 0; i < data.length; i++){
                            //decrypt msg
                            var decipher = crypto.createDecipher(algorithm, password)
                            var dec = decipher.update(data[i].text,'hex','utf8')
                            dec += decipher.final('utf8');
                            data[i].text = dec;
                        }
                        return res.send(data);
                    }
            });
        })
    });
    app.get('/api/msg/:from/:to/:offset', passport.authenticate('bearer', { session: false }), function (req, res, next) {
        db.model('messages').count({$or: [
                {$and: [{to: req.params.from}, {from: req.params.to}]}, 
                {$and: [{from: req.params.from}, {to: req.params.to}]}
            ]}, function(err, data){
                if((data - 40) > req.params.offset){
                    db.model('messages').find({$or: [
                        {$and: [{to: req.params.from}, {from: req.params.to}]}, 
                        {$and: [{from: req.params.from}, {to: req.params.to}]}
                    ]}).populate('from').skip(data - req.params.offset - 40).limit(40).exec(function (err, data) {
                            if (err) next(err);
                            if(data){
                                for(var i = 0; i < data.length; i++){
                                    //decrypt msg
                                    var decipher = crypto.createDecipher(algorithm, password)
                                    var dec = decipher.update(data[i].text,'hex','utf8')
                                    dec += decipher.final('utf8');
                                    data[i].text = dec;
                                }
                            }
                            res.send(data);
                    });
                }else{
                    res.send([]);
                }
        });
    });
    app.post('/api/msg', passport.authenticate('bearer', { session: false }), handlers.messages.create);
};