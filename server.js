var express         = require('express');
var fs              = require('fs');
var crypto          = require('crypto');
var favicon         = require('serve-favicon');
var bodyParser      = require('body-parser');
var methodOverride  = require('method-override');
var path            = require('path');
var passport        = require('passport');
var routes          = require('./routes'); // Файл с роутам
var db              = require('./libs/mongoose');
var log             = require('./libs/log')(module);
var config          = require('./libs/config');

var app = express();

app.use(express.static(__dirname + '/public'));
app.use(bodyParser()); // стандартный модуль, для парсинга JSON в запросах
app.use(methodOverride()); // поддержка put и delete
app.use(passport.initialize());

app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'OPTIONS,GET,POST,PUT,DELETE');
    res.header("Access-Control-Allow-Headers", "Content-Type, Authorization, X-Requested-With");
    if ('OPTIONS' == req.method){
        return res.send(200);
    }
    next();
});
/*
app.use(function(req, res, next){
    res.status(404);
    log.debug('Not found URL: %s',req.url);
    res.send({ error: 'Not found' });
    return;
});*/

app.use(function(err, req, res, next){
    res.status(err.status || 500);
    log.error('Internal error(%d): %s',res.statusCode,err.message);
    res.send({ error: err.message });
    return;
});

app.get('/api', function (req, res) {
    res.send('API is running');
});

// Инициализируем Handlers
var handlers = {
    messages: require('./handlers/messages'),
    usergroup: require('./handlers/usergroup'),
    todo: require('./handlers/todo'),
    user: require('./handlers/user')
}

// Метод запуска нашего сервера
function run() {
    routes.setup(app, handlers); // Связуем Handlers с Routes
    db.init(path.join(__dirname, "models"), function (err, data) {
        //Выводим сообщение об успешной инициализации базы данных
        log.info("All the models are initialized");
        
        var oauth2 = require('./libs/oauth2');
        app.post('/oauth/token', oauth2.token);
        
        require('./libs/oauth');
        
        http = require('http').Server(app);
        io = require('socket.io')(http);

        var usersOnline = {};

        io.on('connection', function(socket){

            socket.on('online', function(data){
                if(data in usersOnline){
                    //callback(false);
                }else{
                    //callback(true);
                    socket.user_id = data;
                    usersOnline[socket.user_id] = socket;
                }
                setTimeout(function(){
                    socket.broadcast.emit('users:online', Object.keys(usersOnline));
                    socket.emit('users:online', Object.keys(usersOnline));
                }, 500);

            });   

            socket.on('logout', function(data){
                if(!usersOnline) return;
                delete usersOnline[data];
                setTimeout(function(){
                    socket.broadcast.emit('users:online', Object.keys(usersOnline));
                    socket.emit('users:online', Object.keys(usersOnline));
                }, 500);

            });  

            socket.on('msg:send', function(data){

                if(data.type == 'file'){
                    
                    var filename = data.filename.split('.')[0] + crypto.randomBytes(4).toString('hex')+ "." + data.filename.split('.')[1];

                    fs.writeFile(path.dirname() + "/public/files/" + filename, data.text, 'binary', function(err) {
                        if(err) {
                            console.log(err);
                        }else{
                            data.text = 'http://'+config.get('host')+':'+config.get('port')+'/files/' + filename;

                            db.model("messages").create(data, function (err, data) {
                                if (err) {
                                    next(err);
                                }

                                db.model("messages").find({_id: data._id}).populate('from').exec(function(err, data){
                                    if (err) {
                                        next(err);
                                    }

                                    if(data[0].to in usersOnline){
                                        usersOnline[data[0].to].emit('msg:private', data[0]);
                                    }
                                    if(usersOnline[data[0].from._id])
                                        usersOnline[data[0].from._id].emit('msg:get', data[0]);
                                })
                            });
                        }
                    });
                }else{
                    if(data.text){
                        //encrypt msg
                        var algorithm = 'aes-256-ctr';
                        var password = 'jJHhj3K';
                        
                        var cipher = crypto.createCipher(algorithm, password)
                        var crypted = cipher.update(data.text,'utf8','hex')
                        crypted += cipher.final('hex');
                        data.text = crypted;
                        
                        db.model("messages").create(data, function (err, data) {
                            if (err) {
                                next(err);
                            }

                            db.model("messages").find({_id: data._id}).populate('from').exec(function(err, data){
                                if (err) {
                                    next(err);
                                }
                                //decrypt msg
                                var decipher = crypto.createDecipher(algorithm, password)
                                var dec = decipher.update(data[0].text,'hex','utf8')
                                dec += decipher.final('utf8');
                                data[0].text = dec;
                                
                                if(data[0].to in usersOnline){
                                    usersOnline[data[0].to].emit('msg:private', data[0]);
                                }
                                if(usersOnline[data[0].from._id])
                                    usersOnline[data[0].from._id].emit('msg:get', data[0]);
                            })
                        });
                    }
                }
            });

            socket.on('disconnect', function(data){
                if(!usersOnline) return;
                delete usersOnline[socket.user_id];
                setTimeout(function(){
                    socket.broadcast.emit('users:online', Object.keys(usersOnline));
                    socket.emit('users:online', Object.keys(usersOnline));
                }, 500);
            })
        });

        http.listen(config.get('port'), function () {
            // Сервер запущен
            log.info("App running on port:" + config.get('port'));
        });
    });
}

if (module.parent) {
  //Если server.js запущен как модуль, то отдаем модуль с методом run
  module.exports.run = run;
} else {
  //Иначе стартуем сервер прямо сейчас
  run();
}