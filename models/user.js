var path        = require('path');
var crypto      = require('crypto');

module.exports = function (mongoose) {

  //Объявляем схему для Mongoose
    var Schema = new mongoose.Schema({
        username: { type: String, required: true, unique: true },
        firstname: { type: String, required: true },
        lastname: { type: String, required: true },
        email: { type: String, required: true, unique: true },
        group: {type: mongoose.Schema.Types.ObjectId, ref: 'usergroup'},
        todo: {type: mongoose.Schema.Types.ObjectId, ref: 'todo'},
        hashedPassword: {
            type: String,
            required: true
        },
        salt: {
            type: String,
            required: true
        },
        created: {
            type: Date,
            default: Date.now
        }
    });
  
    Schema.methods.encryptPassword = function(password) {
        return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
        //more secure - return crypto.pbkdf2Sync(password, this.salt, 10000, 512);
    };

    Schema.virtual('userId')
        .get(function () {
            return this.id;
        });

    Schema.virtual('password')
        .set(function(password) {
            this._plainPassword = password;
            this.salt = crypto.randomBytes(32).toString('base64');
            //more secure - this.salt = crypto.randomBytes(128).toString('base64');
            this.hashedPassword = this.encryptPassword(password);
        })
        .get(function() { return this._plainPassword; });


    Schema.methods.checkPassword = function(password) {
        return this.encryptPassword(password) === this.hashedPassword;
    };

    // Инициализируем модель с именем файла, в котором она находится
    return mongoose.model(path.basename(module.filename, '.js'), Schema);
};