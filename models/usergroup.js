var path = require('path');

module.exports = function (mongoose) {

  //Объявляем схему для Mongoose
  var Schema = new mongoose.Schema({
    title: {type: String, required: true},
    role: {type: String, default: 'Пользователь'}
  });

  // Инициализируем модель с именем файла, в котором она находится
  return mongoose.model(path.basename(module.filename, '.js'), Schema);
};