var path = require('path');

module.exports = function (mongoose) {

  //Объявляем схему для Mongoose
  var Schema = new mongoose.Schema({
    text: {type: String, required: true},
    _user: {type: mongoose.Schema.Types.ObjectId, ref: 'user'},
    status: {type: Number, min: 0, max: 1, default: 0},
    start: {type: Date},
    end: {type: Date}
  });

  // Инициализируем модель с именем файла, в котором она находится
  return mongoose.model(path.basename(module.filename, '.js'), Schema);
};