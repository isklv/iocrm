var path = require('path');

module.exports = function (mongoose) {

  //Объявляем схему для Mongoose
  var Schema = new mongoose.Schema({
    text: { type: String, required: true },
    from: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    to: { type: mongoose.Schema.Types.ObjectId, ref: 'user'},
    created: {
        type: Date,
        default: Date.now
    }
  });

  // Инициализируем модель с именем файла, в котором она находится
  return mongoose.model(path.basename(module.filename, '.js'), Schema);
};