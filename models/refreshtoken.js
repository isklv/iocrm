var path        = require('path');

module.exports = function (mongoose) {

  //Объявляем схему для Mongoose
    var Schema = new mongoose.Schema({
        userId: {
            type: String,
            required: true
        },
        clientId: {
            type: String,
            required: true
        },
        token: {
            type: String,
            unique: true,
            required: true
        },
        created: {
            type: Date,
            default: Date.now
        }
    });
// Инициализируем модель с именем файла, в котором она находится
    return mongoose.model(path.basename(module.filename, '.js'), Schema);
};
