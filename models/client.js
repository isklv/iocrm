var path        = require('path');

module.exports = function (mongoose) {

  //Объявляем схему для Mongoose
    var Schema = new mongoose.Schema({
        name: {
            type: String,
            unique: true,
            required: true
        },
        clientId: {
            type: String,
            unique: true,
            required: true
        },
        clientSecret: {
            type: String,
            required: true
        }
    });
  
    // Инициализируем модель с именем файла, в котором она находится
    return mongoose.model(path.basename(module.filename, '.js'), Schema);
};


